//
//  ContactsTableViewController.swift
//  GetUsersColucciaMaxi
//
//  Created by User on 6/18/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class ContactsTableViewController: BaseTableViewController {

    var contacts = Dictionary<String,Array<Contact>>()
    var indexContacts = Array<String>()
    var contactSelected:Contact?
    
    func processContacts(contactsArray:Array<Contact>?)
    {
        contacts.removeAll()
        indexContacts.removeAll()
        
        guard let contactsArray = contactsArray else
        {
            return
        }
        
         var letters = Set<String>()
        
        for contact in contactsArray
        {
            let contactName = contact.completeName()
            let firstLetter = String(contactName[contactName.startIndex])
            var contactsByLetter = contacts[firstLetter]
            if var contactsByLetter = contactsByLetter
            {
                contactsByLetter.append(contact)
                contacts[firstLetter] = contactsByLetter.sorted(by: { (c1:Contact, c2:Contact) -> Bool in
                        
                        return c1.completeName().compare(c2.completeName()) == .orderedAscending
                })
            }
            else
            {
                contactsByLetter = Array<Contact>()
                contactsByLetter?.append(contact)
                contacts[firstLetter] = contactsByLetter
            }
                
            letters.insert(firstLetter)
        }
        
        
        indexContacts = letters.sorted()
    }
    
    func reloadContacts()
    {
        LibraryAPI.sharedInstance.getContacts { (contactsArray:Array<Contact>?, error:Error?) in
            
            
            DispatchQueue.main.async
            {
                self.hideLoadingView()
                self.refreshControl?.endRefreshing()
                if let contactsRecovered = contactsArray
                {
                    self.processContacts(contactsArray: contactsRecovered)
                }
                self.tableView.reloadData()
                
                if let error = error
                {
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        //Execute with delay of 1 second to do not conflict with self.refreshControl?.endRefreshing()
                        self.navigationController?.present(alert, animated: true, completion: nil)
                    })
                    
                }
                
            }
            
        }
    }
    
    func addRefreshControl()
    {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(reloadContacts), for: UIControlEvents.valueChanged)
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 57
        
        self.addRefreshControl()
        self.showLoadingView()
        self.reloadContacts()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func sectionIndexTitles(for tableView: UITableView) -> [String]?
    {
            return indexContacts
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        if indexContacts.count > 0
        {
            self.tableView.separatorStyle = .singleLine
            self.tableView.backgroundView = nil
            
            return indexContacts.count
        }
        else
        {
            self.tableView.separatorStyle = .none
            
            let noElementsLabel = UILabel()
            noElementsLabel.backgroundColor = UIColor.white
            noElementsLabel.textColor = UIColor.black
            noElementsLabel.text = "No data is currently available. Please pull down to refresh."
            noElementsLabel.textAlignment = .center
            noElementsLabel.font = UIFont.italicSystemFont(ofSize: 20.0)
            noElementsLabel.numberOfLines = 0
            noElementsLabel.sizeToFit()
            
            self.tableView.backgroundView = noElementsLabel
            
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let firstLetter = indexContacts[section]
        if let contactsByLetter = contacts[firstLetter]
        {
            return contactsByLetter.count
        }
        
        return 0
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return indexContacts[section]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCellIdentifier", for: indexPath) as! ContactTableViewCell

        // Configure the cell...
        let firstLetter = indexContacts[indexPath.section]
        let contactsByLetter = contacts[firstLetter]
        let contact = contactsByLetter![indexPath.row]
        
        cell.infoLabel.text = contact.completeName()
        
        if let URLStringImg = contact.thumbPhotoURL
        {
            cell.photoImgView.sd_setImage(with: URL(string:URLStringImg), placeholderImage: UIImage(named: "photoUserPlaceHolder"))
        }
        
        return cell
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        switch segue.identifier!
        {
            case "DetailContactSegueIdentifier":
                let firstLetter = indexContacts[self.tableView.indexPathForSelectedRow!.section]
                let contactsByLetter = contacts[firstLetter]
                contactSelected = contactsByLetter![self.tableView.indexPathForSelectedRow!.row]
                (segue.destination as! DetailContactTableViewController).contact = contactSelected
            break
            
            default: break
        }
    }
    

}
