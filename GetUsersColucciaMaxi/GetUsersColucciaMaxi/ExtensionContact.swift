//
//  ExtensionContact.swift
//  GetUsersColucciaMaxi
//
//  Created by User on 6/22/17.
//  Copyright © 2017 Max. All rights reserved.
//

import Foundation

extension Contact
{
    func completeName() -> String
    {
        var completeName = ""
        if let firstName = firstName
        {
            completeName += firstName
        }
        if let lastName = lastName
        {
            completeName += " " + lastName
        }
        
        if completeName.characters.count == 0
        {
            completeName = " "
        }
        
        return completeName
    }
}
