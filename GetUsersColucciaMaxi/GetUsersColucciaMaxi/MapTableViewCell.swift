//
//  MapTableViewCell.swift
//  GetUsersColucciaMaxi
//
//  Created by User on 6/20/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit
import MapKit

class MapTableViewCell: UITableViewCell {

    @IBOutlet weak var mapAddress: MKMapView!
    @IBOutlet weak var typeLabel: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
