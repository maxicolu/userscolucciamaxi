//
//  ContactDAO.swift
//  GetUsersColucciaMaxi
//
//  Created by User on 6/22/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit
import CoreData

class ContactDAO: AnyObject
{
    func getContacts() -> Array<Contact>?
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
        
        do
        {
            let fetchedObjects = try context.fetch(fetchRequest)
            let contacts = fetchedObjects as! [Contact]
            
            return contacts
        }
        catch
        {
            print(error.localizedDescription)
            
            return nil
        }
    }
    
}
