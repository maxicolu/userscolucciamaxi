//
//  ActionTableViewCell.swift
//  GetUsersColucciaMaxi
//
//  Created by User on 6/20/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class ActionTableViewCell: UITableViewCell {

    let actionButton = UIButton(type: .custom)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.textLabel?.font = UIFont.systemFont(ofSize: 13.0)
        
        actionButton.frame = CGRect(x: 0, y: 0, width: 40.0, height: 40.0)
        self.accessoryView = actionButton
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
