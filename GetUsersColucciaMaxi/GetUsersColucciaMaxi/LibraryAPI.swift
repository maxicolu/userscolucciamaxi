//
//  LibraryAPI.swift
//  GetUsersColucciaMaxi
//
//  Created by User on 6/17/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireCoreData

class LibraryAPI: AnyObject
{
    static let sharedInstance = LibraryAPI()
    
    private let URLBase = "https://private-d0cc1-iguanafixtest.apiary-mock.com"
    private let contacts = "/contacts"
    
    let internetError = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Please check your internet connection or try again later"])
    
    func getContacts( completion:@escaping ((Array<Contact>?,Error?) -> ()) )
    {
        guard (UIApplication.shared.delegate as! AppDelegate).networkManager?.isReachable == true else
        {
            completion(ContactDAO().getContacts(),internetError)
            return
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        Alamofire.request(URLBase + contacts).validate(statusCode: 200..<300).validate(contentType: ["application/json"]).responseInsert(context: context, type: Many<Contact>.self) { response in
            switch response.result
            {
                case let .success(contacts):
                    do
                    {
                        try context.save()
                    }
                    catch
                    {
                        completion(contacts.array,error)
                    }
                    completion(contacts.array,nil)
                break
           
                case .failure(let error):
                        completion(ContactDAO().getContacts(),error)
                break
            }
        }
        
    }
    
    func getContact(contactID:String, completion:@escaping ((Contact?,Error?) -> ()) )
    {
        guard (UIApplication.shared.delegate as! AppDelegate).networkManager?.isReachable == true else
        {
            completion(nil,internetError)
            return
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        Alamofire.request(URLBase + contacts + "/" + contactID).validate(statusCode: 200..<300).validate(contentType: ["application/json"]).responseInsert(context: context, type:Contact.self) { response in
            switch response.result
            {
                case let .success(contact):
                    do
                    {
                        try context.save()
                    }
                    catch
                    {
                        completion(contact,error)
                    }
                    completion(contact,nil)
                break
                
                case .failure(let error):
                    completion(nil,error)
                break
            }
        }
        
    }
    
}
