//
//  DetailContactTableViewController.swift
//  GetUsersColucciaMaxi
//
//  Created by User on 6/19/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit
import SDWebImage
import MapKit

class DetailContactTableViewController: BaseTableViewController {

    var contact:Contact!
    var contactBirthday:String?
    var phones = Array<Phone>()
    var addresses = Dictionary<String,String>()
    var typesAddress = Array<String>()
    
    @IBOutlet weak var contactNameLabel: UILabel!
    @IBOutlet weak var photoImgView: UIImageView!
    
    func processAddresses()
    {
        addresses.removeAll()
        
        guard let allAddress = contact.addresses?.allObjects as? [Address] else
        {
            typesAddress = Array<String>()
            return
        }
        
        var keysAdresses = Set<String>()
        for address in allAddress
        {
            for attr in  Address.entity().attributesByName
            {
                if let value = address.value(forKey: attr.key)
                {
                    keysAdresses.insert(attr.key)
                    addresses[attr.key] = value as? String
                }
            }
        }
        typesAddress = keysAdresses.sorted()
    }
    
    func processPhones()
    {
        phones.removeAll()
        guard let phonesArray = contact.phones else
        {
            return
        }
        
        for phone in (phonesArray.allObjects as! [Phone])
        {
            if let number = phone.number,number.characters.count > 0
            {
                phones.append(phone)
            }
        }
        
    }
    
    func processBirthday()
    {
        contactBirthday = nil
        guard let birthday = contact.birthday else
        {
            return
        }
        
       let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: birthday)
        if let date = date
        {
            dateFormatter.dateStyle = .medium
            contactBirthday = dateFormatter.string(from: date)
        }
        
    
    }
    
    func configHeader()
    {
        photoImgView.layer.cornerRadius = photoImgView.frame.size.width/2.0
        photoImgView.layer.masksToBounds = true
        if let photoURLString = contact.photoURL,let photoURL = URL(string:photoURLString)
        {
            photoImgView.sd_setImage(with: photoURL, placeholderImage: UIImage(named: "photoUserPlaceHolder"))
        }
        
        contactNameLabel.text = contact.completeName()
    }
    
    func reloadContact()
    {
        self.showLoadingView()
        LibraryAPI.sharedInstance.getContact(contactID: contact.contactID!) { (contactResult:Contact?, error:Error?) in
            
            if let contactR = contactResult
            {
                self.contact = contactR
            }
            
            DispatchQueue.main.async
            {
                self.hideLoadingView()
                
                if let error = error
                {
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
                self.processBirthday()
                self.processPhones()
                self.processAddresses()
                self.configHeader()
                
                self.tableView.reloadData()
            }
            
        }
    }
    
    func showlocationInMap(locationString:String,map:MKMapView)
    {
        let geocoder:CLGeocoder = CLGeocoder()
        geocoder.geocodeAddressString(locationString + " Buenos Aires") { (placemarks:[CLPlacemark]?, error:Error?) in
        
            if let placemarks = placemarks, placemarks.count > 0
            {
                let topResult:CLPlacemark = placemarks[0]
                let placemark: MKPlacemark = MKPlacemark(placemark: topResult)
                
                var region: MKCoordinateRegion = map.region
                region.center = (placemark.location?.coordinate)!
                region.span.longitudeDelta /= 350.0
                region.span.latitudeDelta /= 350.0
                map.setRegion(region, animated: false)
                map.addAnnotation(placemark)
            }
            
        }
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140
        
        self.reloadContact()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        switch section
        {
            case 0:
                if contactBirthday != nil
                {
                    return 1
                }
            break
            
            case 1:
                return phones.count
            
        case 2:
                return typesAddress.count
            
            default:
                return 0
        }
        
        return 0
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch section
        {
            case 0:
                if contactBirthday != nil
                {
                    return "Birthday"
                }
            break
            
            case 1:
                if phones.count > 0
                {
                    return "Phones"
                }
            break
            
        case 2:
            if typesAddress.count > 0
            {
                return "Addresses"
            }
            break
            
            default:
                return nil
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        // Configure the cell...
        switch indexPath.section
        {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "birthdayTableViewCellIdentifier", for: indexPath)
                cell.selectionStyle = .none
                cell.textLabel?.font = UIFont.systemFont(ofSize: 13.0)
                cell.textLabel?.text = contactBirthday
                return cell
            
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "ActionTableViewCellIdentifier", for: indexPath) as! ActionTableViewCell
                cell.textLabel?.text = phones[indexPath.row].type! + ": " + phones[indexPath.row].number!
                cell.actionButton.setImage(UIImage(named:"callImg"), for: .normal)
                cell.actionButton.removeTarget(self, action: nil, for: .touchUpInside)
                cell.actionButton.addTarget(self, action: #selector(callAction), for:.touchUpInside)
                cell.actionButton.tag = indexPath.row
                return cell
            
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "MapTableViewCellIdentifier", for: indexPath) as! MapTableViewCell
                let typeAddress = typesAddress[indexPath.row]
                cell.typeLabel.text = typeAddress + ": " + addresses[typeAddress]!
                self.showlocationInMap(locationString:addresses[typeAddress]! , map: cell.mapAddress)
                return cell
            
            default:
                return tableView.dequeueReusableCell(withIdentifier: "birthdayTableViewCellIdentifier", for: indexPath)
        }
        
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - Action methods
    func callAction(sender:UIButton)
    {
        let phone = phones[sender.tag]
        if let url = URL(string: "tel://" + phone.number!), UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url)
        }
        else
        {
            let alert = UIAlertController(title: "Error", message: "This feature is not available on your device", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.navigationController?.present(alert, animated: true, completion: nil)
        }
    }
    
}
